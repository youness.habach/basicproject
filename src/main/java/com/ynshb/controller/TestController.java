package com.ynshb.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@RestController
@RequestMapping("/api/v2")
public class TestController {

    private static Random rnd = new Random();

    @GetMapping
    @RequestMapping(value = "/all")
    public Map<String, String> getAll() {
        Map<String, String> result = new HashMap<>();
        for (int i = 0; i < 10; i++) {
            result.put("key_" + i, "value_" + i + "_" + Math.abs(rnd.nextInt(100)));
        }
        return result;
    }

    @GetMapping
    @RequestMapping(value = "/random")
    public Map<String, Integer> getRandom() {
        Map<String, Integer> result = new HashMap<>();
        for (int i = 0; i < 10; i++) {
            result.put("key_" + i, rnd.nextInt());
        }
        return result;
    }
}
